FROM openjdk:11-jre-slim
RUN adduser --disabled-password --gecos "" myuser
USER myuser
COPY /target/palantir-*.jar /usr/src/palantir/palantir.jar
WORKDIR /usr/src/palantir
EXPOSE 8080
CMD ["java", "-jar", "palantir.jar"]