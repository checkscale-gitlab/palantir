package gr.serresparc.palantir.dummy;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RegExDummyTest
{
    @Test
    void isNameTest1()
    {
        String name = "";
        RegExDummy regExDummy = new RegExDummy();
        assertEquals(false, regExDummy.isName(name), "Regular Expression failed!");
    }

    @Test
    void isNameTest2()
    {
        String name = "sDall";
        RegExDummy regExDummy = new RegExDummy();
        assertEquals(false, regExDummy.isName(name), "Regular Expression failed!");
    }

    @Test
    void isNameTest3()
    {
        String name = "Idlib";
        RegExDummy regExDummy = new RegExDummy();
        assertEquals(true, regExDummy.isName(name), "Regular Expression failed!");
    }

    @Test
    void isNameTest4()
    {
        String name = "Dasd ";
        RegExDummy regExDummy = new RegExDummy();
        assertEquals(false, regExDummy.isName(name), "Regular Expression failed!");
    }

    @Test
    void isNameTest5()
    {
        String name = "3421";
        RegExDummy regExDummy = new RegExDummy();
        assertEquals(false, regExDummy.isName(name), "Regular Expression failed!");
    }

    @Test
    void isNameTest6()
    {
        String name = ";lf32#@";
        RegExDummy regExDummy = new RegExDummy();
        assertEquals(false, regExDummy.isName(name), "Regular Expression failed!");
    }

    @Test
    void isNameTest7()
    {
        String name = "SAFFSD";
        RegExDummy regExDummy = new RegExDummy();
        assertEquals(false, regExDummy.isName(name), "Regular Expression failed!");
    }

    @Test
    void isNameTest8()
    {
        String name = "  Athens";
        RegExDummy regExDummy = new RegExDummy();
        assertEquals(false, regExDummy.isName(name), "Regular Expression failed!");
    }

    @Test
    void isNameTest9()
    {
        String name = null;
        RegExDummy regExDummy = new RegExDummy();
        assertEquals(false, regExDummy.isName(name), "Regular Expression failed!");
    }

    @Test
    void hasPunctuation1()
    {
        String name = null;
        RegExDummy regExDummy = new RegExDummy();
        assertEquals(false, regExDummy.hasPunctuation(name), "Regular Expression failed!");
    }

    @Test
    void hasPunctuation2()
    {
        String name = "  Athens.";
        RegExDummy regExDummy = new RegExDummy();
        assertEquals(true, regExDummy.hasPunctuation(name), "Regular Expression failed!");
    }

    @Test
    void hasPunctuation3()
    {
        String name = "  Athens.";
        RegExDummy regExDummy = new RegExDummy();
        assertEquals(true, regExDummy.hasPunctuation(name), "Regular Expression failed!");
    }

    @Test
    void hasPunctuation4()
    {
        String name = "  Athen s.";
        RegExDummy regExDummy = new RegExDummy();
        assertEquals(true, regExDummy.hasPunctuation(name), "Regular Expression failed!");
    }

    @Test
    void hasPunctuation5()
    {
        String name = "  Athens.";
        RegExDummy regExDummy = new RegExDummy();
        assertEquals(true, regExDummy.hasPunctuation(name), "Regular Expression failed!");
    }

    @Test
    void hasPunctuation6()
    {
        String name = "  Athens.";
        RegExDummy regExDummy = new RegExDummy();
        assertEquals(true, regExDummy.hasPunctuation(name), "Regular Expression failed!");
    }

    @Test
    void hasPunctuation7()
    {
        String name = " .yomama";
        RegExDummy regExDummy = new RegExDummy();
        assertEquals(false, regExDummy.hasPunctuation(name), "Regular Expression failed!");
    }

    @Test
    void hasPunctuation8()
    {
        String name = "  asds?";
        RegExDummy regExDummy = new RegExDummy();
        assertEquals(true, regExDummy.hasPunctuation(name), "Regular Expression failed!");
    }

    @Test
    void hasPunctuation9()
    {
        String name = "  s!";
        RegExDummy regExDummy = new RegExDummy();
        assertEquals(true, regExDummy.hasPunctuation(name), "Regular Expression failed!");
    }
}
