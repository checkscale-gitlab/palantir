package gr.serresparc.palantir.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ConflictsTest
{

    Conflicts listOfConflicts = new Conflicts();
    List<String> possibleKeys = new ArrayList<>(Arrays.asList("open", "militia", "beheads"));
    List<String> possibleKeys1 = new ArrayList<>(Arrays.asList("open", "soldiers", "fire"));
    String type;

    @BeforeEach
    void setVariables()
    {
        listOfConflicts.setlistOfConflicts();
        String type = "";
    }

    @Test
    void getTypeOfConflict()
    {
        List<String> conflict = new ArrayList<>();
        conflict.add("mellow");
        conflict.add("killing");
        conflict.add("juice");
        assertEquals("Responsible: Unknown party<br>Type: Killing", listOfConflicts.getTypeOfConflict
                (listOfConflicts.findKeyWords(conflict)));
    }

    @Test
    void getTypeOfConflictWithMultipleConflicts()
    {
        List<String> conflict = new ArrayList<>();
        conflict.add("mellow");
        conflict.add("killing");
        conflict.add("juice");
        conflict.add("attack");
        conflict.add("bombing");
        assertEquals("Responsible: Unknown party<br>Type: IED Bombing(s)", listOfConflicts.getTypeOfConflict
                (listOfConflicts.findKeyWords(conflict)));
    }

    @Test
    void getTypeOfConflictWithoughtARegisteredConflict()
    {
        List<String> conflict = new ArrayList<>();
        conflict.add("mellow");
        conflict.add("juice");
        assertEquals("Responsible: Unknown party<br>Type: Unknown conflict", listOfConflicts.getTypeOfConflict
                (listOfConflicts.findKeyWords(conflict)));
    }

    @Test
    void getTypeOfConflictWithDoubleCheck()
    {
        List<String> conflict = new ArrayList<>();
        conflict.add("mellow");
        conflict.add("juice");
        assertEquals("Responsible: Unknown party<br>Type: Unknown conflict", listOfConflicts.getTypeOfConflict
                (listOfConflicts.findKeyWords(conflict)));
        conflict.add("mellow");
        conflict.add("killing");
        conflict.add("juice");
        assertEquals("Responsible: Unknown party<br>Type: Killing", listOfConflicts.getTypeOfConflict
                (listOfConflicts.findKeyWords(conflict)));
    }

    @Test
    void findKeyWords()
    {
        listOfConflicts.findKeyWords(possibleKeys);
        assertEquals("opened", listOfConflicts.getAction());
        assertEquals("militia", listOfConflicts.getResponsible());
    }

    @Test
    void findResponsActionType()
    {
        String type = "";
        type = listOfConflicts.findKeyWords(possibleKeys);
        assertEquals("beheads", type);
        assertEquals("opened", listOfConflicts.getAction());
        assertEquals("militia", listOfConflicts.getResponsible());

    }

    @Test
    void TestWithFullConflictMatcher()
    {

        type = listOfConflicts.getTypeOfConflict(listOfConflicts.findKeyWords(possibleKeys1));
        assertEquals("opened", listOfConflicts.getAction());
        assertEquals("Responsible: soldiers<br>Type: opened fire", type);
        assertEquals("soldiers", listOfConflicts.getResponsible());
        assertEquals("opened", listOfConflicts.getAction());
    }
}