package gr.serresparc.palantir.dao;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import gr.serresparc.palantir.model.User;
import gr.serresparc.palantir.mongodb.Database;
import org.bson.Document;
import org.junit.Ignore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

@Disabled
public class UserDAOTest
{
    String USER_COLLECTION = "Users";
    Database database = new Database();
    MongoDatabase mongoDb = database.connectToAtlas();
    MongoCollection<Document> mongoCollection = mongoDb.getCollection(USER_COLLECTION);
    User user;
    UserDAO userDAO = new UserDAO();
    MongoCursor mongoCursor = mongoCollection.find().iterator();
    int expInt;


    @BeforeEach
    public void setUpVars()
    {
        user = new User("12345", "", "",
                        "", "", "kilkis", "greece");
        userDAO.createUser(user);
        expInt = 1;
        while (mongoCursor.hasNext())
        {
            expInt++;
            mongoCursor.next();

        }
    }

    @Disabled
    @Test
    public void checkIfAfterReadAndCreateTheOutputListHasTheSameSizeWithDocumentsOfMongodb()
    {
        List<User> actualList = userDAO.readUsers();
        int actInt = actualList.size();
        assertEquals(expInt, actInt);

    }

    @Ignore
    @Test
    public void checkForTheCorrectLogIn()
    {
        boolean actUser = userDAO.logIn("chrisalo", "12345");

    }


}