package gr.serresparc.palantir.twitter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.util.ReflectionTestUtils;
import twitter4j.Status;
import twitter4j.TwitterException;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TwitterConfigurationTest
{
    TwitterConfiguration twitterConfiguration;

    @BeforeEach
    public void setUp()
    {
        twitterConfiguration = new TwitterConfiguration();
        ReflectionTestUtils.setField(twitterConfiguration, "", "");
        ReflectionTestUtils.setField(twitterConfiguration, "",
                                     "");
        ReflectionTestUtils.setField(twitterConfiguration, "", "" +
                                                                              "");
        ReflectionTestUtils.setField(twitterConfiguration, "",
                                     "");
    }

    @Test
    void fetchUserTimeline() throws TwitterException
    {
        List<Status> statuses = twitterConfiguration.twitter(twitterConfiguration.twitterFactory())
                .getUserTimeline("@Conflicts"); // The User id of the account is 2566535282l
        long id = statuses.get(3).getUser().getId();

        assertEquals(2566535282L, id);
    }
}
