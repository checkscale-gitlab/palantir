package gr.serresparc.palantir.mongodb;

import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import org.junit.jupiter.api.Test;

import java.util.logging.Logger;

import static org.junit.Assert.assertNotNull;

class DatabaseTest
{
    private static final Logger logger = Logger.getLogger(DatabaseTest.class.getName());

    Database mongodb = new Database();

    MongoDatabase mongoDatabase = mongodb.connectToAtlas();
    MongoClient client = mongodb.getMongoClient();


    @Test
    void testConnection()
    {
        MongoCursor<String> dbsCursor = client.listDatabaseNames().iterator();
        assertNotNull(dbsCursor);
    }

    @Test
    void AtlasConnectionTest()
    {
        MongoDatabase db = mongodb.connectToAtlas();
        MongoCollection atlasClient = db.getCollection("Conflicts");
        FindIterable dbsCursor = atlasClient.find();
        logger.info("Cursor: " + dbsCursor);
        assertNotNull(dbsCursor);
    }
}