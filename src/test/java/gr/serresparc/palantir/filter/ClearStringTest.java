package gr.serresparc.palantir.filter;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ClearStringTest
{
    ClearString clearString = new ClearString();

    @Test
    void clearTextFormat()
    {

        List<String> expected = singletonList("al-Bukamal");
        List<String> actual = clearString.clearTextFormat(singletonList("?al-Bukamal!&43"));
        assertEquals(expected, actual);
    }

    @Test
    void clearTextFormat2()
    {
        List<String> exVal = new ArrayList<>();
        exVal.add("Test-");
        exVal.add("text");

        List<String> input = new ArrayList<>();
        input.add("?Te$st-!");
        input.add("@t#e(x)%t");
        List<String> reVal;
        reVal = clearString.clearTextFormat(input);
        assertEquals(exVal, reVal);
    }

    @Test
    void AlreadyClearText()
    {
        List<String> exVal = new ArrayList<>();
        exVal.add("Test");
        exVal.add("text");

        List<String> input = new ArrayList<>();
        input.add("Test");
        input.add("text");
        List<String> reVal;
        reVal = clearString.clearTextFormat(input);
        assertEquals(exVal, reVal);
    }

    @Test
    void stringMatcher()
    {
        assertEquals("open fire", clearString.stringMatcher("open", "fire"));
    }

    @Test
    void stringMatcherED()
    {
        assertEquals("opened fire", clearString.stringMatcher("opened", "fire"));
    }

    @Test
    void capitaliseFirst() {
        ClearString capitaliser = new ClearString();
        assertEquals("Syria",capitaliser.capitaliseFirst("sYriA"));
        assertEquals("Iraq",capitaliser.capitaliseFirst("iRaq"));
        assertEquals("Patates","Patates");
    }
}