package gr.serresparc.palantir.nlp;


import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

@Component
public class StanfordNER
{
    private static final Logger logger = Logger.getLogger(StanfordNER.class.getName());
    private Properties props = new Properties();
    private StanfordCoreNLP pipeline;
    private Annotation document;
    private List<String> words = new ArrayList<>();
    private List<String> posTags = new ArrayList<>();
    private List<String> nerTags = new ArrayList<>();
    private List<CoreMap> sentences = new ArrayList<>();
    private List<String> location = new ArrayList<>();


    public StanfordNER()
    {
        props.setProperty("annotators", "tokenize, ssplit, pos, lemma, ner, parse, dcoref");
        props.setProperty("ner.useSUTime", "false");
        pipeline = new StanfordCoreNLP(props);
    }

    public void getEntities(String text)
    {
        logger.info("Raw text: " + text);
        document = new Annotation(text);
        pipeline.annotate(document);
        sentences = document.get(CoreAnnotations.SentencesAnnotation.class);


        for (CoreMap sentence : sentences)
        {
            for (CoreLabel token : sentence.get(CoreAnnotations.TokensAnnotation.class))
            {
                String word = token.get(CoreAnnotations.TextAnnotation.class);
                logger.info("Text annotation: " + word);

                words.add(word);
                // this is the POS tag of the token
                String pos = token.get(CoreAnnotations.PartOfSpeechAnnotation.class);
                posTags.add(pos);
                // this is the NER label of the token
                String ne = token.get(CoreAnnotations.NamedEntityTagAnnotation.class);
                logger.info("Named Entity: " + ne);

                nerTags.add(ne);
                // this is the whole entity


            }
        }
    }

    public Properties getProps()
    {
        return props;
    }

    public StanfordCoreNLP getPipeline()
    {
        return pipeline;
    }

    public Annotation getDocument()
    {
        return document;
    }

    public List<String> getWords()
    {
        return words;
    }

    public List<String> getPosTags()
    {
        return posTags;
    }

    public List<String> getNerTags()
    {
        return nerTags;
    }

    public List<CoreMap> getSentences()
    {
        return sentences;
    }


    public String extractLocation(String text)
    {


        getEntities(text);

        List<String> ner = getNerTags();
        List<String> words = getWords();
        List<String> pos = getPosTags();
        Iterator nerIt = ner.iterator();

        for (int i = 0; i < ner.size(); i++)
        {
            switch (ner.get(i))
            {
                case ("COUNTRY"):
                case ("CITY"):
                case ("LOCATION"):
                    while (i < ner.size())
                    {
                        try
                        {
                            if (!ner.get(i).equals(ner.get(i + 1)))
                            {
                                return words.get(i);
                            }
                            else
                            {
                                return words.get(i) + " " + words.get(i + 1);
                            }
                        }
                        catch (IndexOutOfBoundsException e)
                        {
                            return words.get(i);
                        }
                    }


            }
        }
        return null;
    }

}

