package gr.serresparc.palantir.parser;


import gr.serresparc.palantir.filter.ClearString;
import gr.serresparc.palantir.model.Tweet;
import gr.serresparc.palantir.nlp.StanfordNER;
import org.jetbrains.annotations.NotNull;
import org.jsoup.Jsoup;
import org.springframework.stereotype.Component;
import twitter4j.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

@Component
public class Parser
{
    private static final Logger logger = Logger.getLogger(Parser.class);

    private final StanfordNER stanfordNER;

    public Parser(StanfordNER stanfordNER)
    {
        this.stanfordNER = stanfordNER;
    }

    /**
     * Parses a JSON file of 20 tweets, fetched with Twitter4j
     *
     * @param listOfTweetsJSON is a List of Status Objects. These are the tweets fetched in JSON format
     * @return tweets a List of 20 instances of the class Tweet by default
     */
    @NotNull
    public List<Tweet> parseJSON(@NotNull List<Status> listOfTweetsJSON, Twitter twitterInstance)
    {
        List<Tweet> tweets = new ArrayList<>();

        for (Status status : listOfTweetsJSON)
        {
            String url = "https://twitter.com/" + status.getUser().getScreenName()
                         + "/status/" + status.getId();
            OEmbedRequest oEmbedRequest;
            OEmbed embed;
            try
            {
                oEmbedRequest = new OEmbedRequest(status.getId(), url);
                embed         = twitterInstance.getOEmbed(oEmbedRequest);
                tweets.add(new Tweet(status.getText(),
                                     status.getId(),
                                     status.getCreatedAt(),
                                     status.getRetweetCount(),
                                     embed.getHtml()));

                logger.info("New parsed Tweet: \"" + Jsoup.parse(embed.getHtml()).text() + "\"");
            }
            catch (TwitterException e)
            {
                logger.error("Failed to parse Tweet status!");
                e.printStackTrace();
            }
        }
        return tweets;
    }

    /**
     * /**
     * Checks every word if its a valid Country or the 1st word if is valid country code
     *
     * @param entities List<String>
     * @return String
     */
    public String locationFound(@NotNull String entities)
    {
        return stanfordNER.extractLocation(entities);
    }

    /**
     * Takes twitter status text and after splitting it, clears it from special characters.
     *
     * @param tweet Twitter status
     * @return Every word of the text clear of special characters
     */
    public List<String> statusTextToParse(@NotNull Tweet tweet)
    {
        List<String> statusText;
        statusText = Arrays.asList(tweet.getTweetText().split(" "));
        ClearString cString = new ClearString();
        statusText = cString.clearTextFormat(statusText);
        return statusText;
    }

    /**
     * Takes a String and looks for conflict related words
     *
     * @param text String
     * @return boolean
     */
    public boolean isConflict(String text)
    {
        StringTokenizer tokenizedString = new StringTokenizer(text, " ");
        List<String> elements = new ArrayList<>();
        final String[] conflictDictionary = {
                "war",
                "dogfights",
                "bloodbath",
                "dogfight",
                "killing",
                "dead",
                "death",
                "attack",
                "attacks",
                "sink",
                "sunk",
                "shoot",
                "shot",
                "exploded",
                "explosion",
                "explosions",
                "injured",
                "injuring",
                "crashed",
                "shooting",
                "hit",
                "hitting",
                "shells",
                "missile",
                "missiles",
                "rocket",
                "rockets",
                "grenade",
                "grenades",
                "shootings",
                "fire",
                "fired",
                "died",
                "killed",
                "lynching",
                "lynched",
                "shootout",
                "bodies",
                "beheads",
                "bombing",
                "bombings",
                "beheaded",
                "beheading",
                "attacked"
        };
        while (tokenizedString.hasMoreTokens())
        {
            elements.add(tokenizedString.nextToken());
        }
        ClearString clearString = new ClearString();
        List<String> stringList = clearString.clearTextFormat(elements);

        for (String keyword : conflictDictionary)
        {
            if (stringList.contains(keyword))
            {
                logger.info("Conflict keyword detected: " + keyword);
                return true;
            }
        }
        return false;
    }

    /**
     * Checks Tweet age
     *
     * @param newTweet  Tweet
     * @param tweetList List<Tweet>
     * @return boolean
     */
    public boolean isNewById(@NotNull Tweet newTweet, @NotNull List<Tweet> tweetList)
    {
        List<Long> idList = new ArrayList<>();
        tweetList.forEach(tweet -> idList.add(tweet.getId()));
        return !idList.contains(newTweet.getId());
    }

}


