package gr.serresparc.palantir.dummy;

import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Dummy class to test Regular Expressions
 */
class RegExDummy
{
    private static final Logger logger = Logger.getLogger(RegExDummy.class.getName());


    /**
     * Takes a String and matches for first capital letter/s ex. Dave, DAVE, not dave.
     *
     * @param name String
     * @return boolean
     */
    final boolean isName(String name)
    {
        Pattern pattern = Pattern.compile("^(?:[A-Z][a-z]{1,30}){1,6}$"); //Regex that matches names
        try
        {
            Matcher matcher = pattern.matcher(name);
            return matcher.find();
        }
        catch (NullPointerException e)
        {
            logger.severe("String is null");
            return false;
        }
    }

    /**
     * Takes a String and matches for punctuation at the start or the finish, ex. ".Dave" or "dave,"
     *
     * @param name String
     * @return boolean
     */
    final boolean hasPunctuation(String name)
    {
        Pattern pattern = Pattern.compile(".{0,}[.!?\\\\-]$"); //Regex that matches names
        try
        {
            Matcher matcher = pattern.matcher(name);
            return matcher.find();
        }
        catch (NullPointerException e)
        {
            logger.severe("String is null");
            return false;
        }
    }
}