package gr.serresparc.palantir.filter;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for removing symbols from a List of Strings.
 */
@Component
public class ClearString
{

    public List<String> clearTextFormat(@NotNull List<String> tokenizedString)
    {
        List<String> clearText = new ArrayList<>();
        for (String s : tokenizedString)
        {
            String word = s.replaceAll("[^A-Za-z-]", ""); // [^A-Za-z0-9]
            clearText.add(word);
        }
        return clearText;
    }

    public String stringMatcher(String s1,String s2){
        StringBuilder sb = new StringBuilder(s1);
        return String.valueOf(sb.append(" " +s2));
    }

    public String capitaliseFirst(String wordToCapitalise1st){
        String word = wordToCapitalise1st.toLowerCase();
        word = word.substring(0,1).toUpperCase() + word.substring(1);
        return  word;
    }
}
