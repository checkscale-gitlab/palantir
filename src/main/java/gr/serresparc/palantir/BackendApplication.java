package gr.serresparc.palantir;


import gr.serresparc.palantir.service.ConflictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


@EnableConfigurationProperties
@SpringBootApplication(exclude = {MongoAutoConfiguration.class})
public class BackendApplication implements CommandLineRunner
{
    private final ScheduledExecutorService executor;
    private final ConflictService conflictService;

    @Autowired
    public BackendApplication(
            ScheduledExecutorService scheduledExecutorService,
            ConflictService conflictService)
    {
        this.executor        = scheduledExecutorService;
        this.conflictService = conflictService;
    }

    public static void main(String[] args)
    {
        SpringApplication.run(BackendApplication.class, args);
    }

    @Override
    public void run(String[] args)
    {
        executor.scheduleAtFixedRate(() -> conflictService.run(), 0, 15, TimeUnit.MINUTES);
    }

    @Configuration
    public static class ApplicationConfiguration
    {
        @Bean
        public ScheduledExecutorService scheduledExecutorService()
        {
            return Executors.newScheduledThreadPool(5);
        }
    }
}
