package gr.serresparc.palantir.model;

public class    User
{

    private String password;
    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private String city;
    private String country;

    public User(String password, String username, String firstName, String lastName, String email, String city,
                String country)
    {
        this.password  = password;
        this.username  = username;
        this.firstName = firstName;
        this.lastName  = lastName;
        this.email     = email;
        this.city      = city;
        this.country   = country;
    }

    public User()
    {
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(String city)
    {
        this.city = city;
    }

    public String getCountry()
    {
        return country;
    }

    public void setCountry(String country)
    {
        this.country = country;
    }
}
