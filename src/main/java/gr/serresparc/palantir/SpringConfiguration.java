package gr.serresparc.palantir;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;


@ConfigurationProperties(prefix = "application.properties")
@Component
public class SpringConfiguration
{
    @Value("${GoogleApiKey}")
    private String GoogleAPIKey;

    @Bean
    public String getGoogleAPIKey() { return GoogleAPIKey; }


}
