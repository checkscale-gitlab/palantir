package gr.serresparc.palantir.dao;

import com.mongodb.client.DistinctIterable;
import com.mongodb.client.MongoCollection;
import gr.serresparc.palantir.model.Conflict;
import gr.serresparc.palantir.model.Country;
import gr.serresparc.palantir.mongodb.Database;
import org.bson.Document;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.regex;

/**
 * Conflict DAO class for MongoDB
 */
@Component
public class ConflictDAO
{
    private static final Logger logger = Logger.getLogger(ConflictDAO.class.getName());
    private static final String CONFLICT_COLLECTION = "Conflicts";
    private final MongoCollection<Document> mongoCollection;

    /**
     * ConflictDAO constructor to initialize a MongoCollection
     */
    public ConflictDAO()
    {
        mongoCollection = new Database().connectToAtlas().getCollection(CONFLICT_COLLECTION);
    }

//    public void insertIntoDatabase(List<Document> document)
//    {
//        try
//        {
//            mongoCollection.insertMany(document);
//            logger.info("New conflicts detected and sent to database!");
//        }
//        catch (Exception e)
//        {
//            logger.severe("Failed to insert documents into database" + e);
//        }
//    }

    /**
     * Takes a Conflict object and creates a MongoDB document
     *
     * @param conflict Conflict
     */
    public void create(@NotNull Conflict conflict)
    {
        Document document = new Document();
        try
        {
            document.put("_id", conflict.getId());
            document.put("location", conflict.getLocation());
            document.put("conflict_type", conflict.getTypeOfConflict());
            document.put("source", conflict.getSource());
            document.put("latitude", conflict.getLatitude());
            document.put("longitude", conflict.getLongitude());
            document.put("time_stamp", conflict.getTimeStamp());
            document.put("embedded_html", conflict.getEmbeddedHtml());
            mongoCollection.insertOne(document);
            logger.info("Conflict object ID: " + conflict.getId() + " created!");
        }
        catch (Exception e)
        {
            logger.severe("Conflict object ID: " + conflict.getId() + " failed to enter database!. Error: " + e);
        }
    }

    /**
     * Returns a list of Conflict objects in JSON format from a MongoDB database
     *
     * @return List<Conflict>
     */
    public List<Conflict> readConflicts()
    {
        List<Conflict> conflictList = new ArrayList<>();
        List<Document> documentList = mongoCollection.find().into(new ArrayList<>());
        for (Document doc : documentList)
        {

            conflictList.add(new Conflict(doc.getLong("_id"),
                                          doc.get("location", String.class),
                                          doc.getString("conflict_type"),
                                          doc.getString("source"),
                                          doc.getDouble("latitude"),
                                          doc.getDouble("longitude"),
                                          doc.getDate("time_stamp"),
                                          doc.getString("embedded_html")));
        }
        return conflictList;
    }

    public List<Country> conflictsPerCountry(@NotNull List<String> countries)
    {
        List<Country> countryList = new ArrayList<>();
        if (!countries.isEmpty())
        {
            for (String country : countries)
            {
                try
                {
                    double lat = mongoCollection.find(eq("location", country)).first().getDouble("latitude");
                    double lng = mongoCollection.find(eq("location", country)).first().getDouble("longitude");
                    long count = mongoCollection.countDocuments(eq("location", country));
                    logger.info("Total reports for [" + lat + "," + lng + " ]: " + count);
                    countryList.add(new Country(lat, lng, country, count));
                }
                catch (NullPointerException e)
                {
                    logger.severe("No data for " + country);
                }
            }
        }
        return countryList;
    }

    public List<Country> conflictsPerCountry()
    {
        List<Country> countryList;
        DistinctIterable<String> allCountries = mongoCollection.distinct("location", String.class);
        List<String> countries = new ArrayList<>();

        for (String country : allCountries)
        {
            if (!country.isEmpty() && !country.isBlank())
            {
                countries.add(country);
            }
        }
        countryList = conflictsPerCountry(countries);
        return countryList;
    }

    /**
     * Checks if Tweet ID is included in the database
     *
     * @param tweetID Long
     * @return boolean
     */
    public boolean hasTweet(@NotNull Long tweetID)
    {
        return mongoCollection.find(regex("source", ".*" + tweetID.toString()))
                .iterator()
                .hasNext();
    }
}
