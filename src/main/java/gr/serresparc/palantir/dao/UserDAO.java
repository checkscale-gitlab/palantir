package gr.serresparc.palantir.dao;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import gr.serresparc.palantir.model.User;
import gr.serresparc.palantir.mongodb.Database;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.mongodb.client.model.Filters.eq;


public class UserDAO
{
    String USER_COLLECTION = "Users";
    Database database = new Database();
    MongoDatabase mongoDb = database.connectToAtlas();
    MongoCollection<Document> mongoCollection = mongoDb.getCollection(USER_COLLECTION);
    Document user = new Document();
    User user1 = new User();

    public UserDAO()
    {
    }

    public void createUser(User user)
    {
        Logger logger = Logger.getLogger(UserDAO.class.getName());
        Document document = new Document();
        document.put("password", user.getPassword());
        document.put("username", user.getUsername());
        document.put("email", user.getEmail());
        document.put("firstname", user.getFirstName());
        document.put("lastname", user.getLastName());
        document.put("city", user.getCity());
        document.put("country", user.getCountry());
        try
        {
            mongoCollection.insertOne(document);
            logger.log(Level.INFO, "User " + user.getEmail() + " inserted into database!");

        }
        catch (Exception e)
        {
            logger.log(Level.SEVERE, "User " + user.getEmail() + "not inserted into database!");
            e.printStackTrace();
        }
    }

    public List<User> readUsers()
    {
        List<User> userList = new ArrayList<>();
        List<Document> documentList = mongoCollection.find().into(new ArrayList<>());
        for (Document doc : documentList)
        {
            userList.add(new User(doc.getString("password"),
                                  doc.getString("username"),
                                  doc.getString("firstname"),
                                  doc.getString("lastname"),
                                  doc.getString("email"),
                                  doc.getString("city"),
                                  doc.getString("country")));
        }

        return userList;
    }


     public boolean logIn(String username,String password) throws NullPointerException{
        Logger logger = Logger.getLogger(UserDAO.class.getName());
        if(passwordCheck(password,username)){
            logger.log(Level.INFO, "User " + username + " logged In!");
            return true;
        }else{
            logger.log(Level.INFO, "Invalid password or username");
            return false;
        }

    }

    private String getPassword(String username)
    {
        user = mongoCollection.find(eq("username", username)).first();
        return user.get("password").toString();
    }

    private boolean passwordCheck(String password, String username)
    {
        return getPassword(username).matches(password);
    }
}
